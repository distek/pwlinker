package main

import (
	"log"
	"os/exec"
	"regexp"
	"strings"
)

func portPathSplit(line string) []string {
	var ret []string

	trimmedLine := strings.TrimLeft(line, " ")

	split := strings.Split(trimmedLine, " ")

	for i := range split {
		if i == 0 {
			ret = append(ret, split[i])
			continue
		}
		var builder strings.Builder

		for _, w := range split[i:] {
			builder.WriteString(w)

			if i <= len(split)-2 {
				builder.WriteRune(' ')
			}
		}

		ret = append(ret, strings.TrimRight(builder.String(), " "))
		break
	}

	return ret
}

// Splits on space unless in quote cause software developers are dicks
func linkPathSplit(line string) []string {
	var ret []string
	trimmedLine := strings.TrimLeft(line, " ")

	split := strings.Split(trimmedLine, " ")

	// pipeThingRx := regexp.MustCompile(`\|->`)

	nextWordIsID := true
	for i, v := range split {
		if strings.Contains(line, "->") {
			// link ID
			if i == 0 {
				ret = append(ret, v)
				continue
			}

			if v == "" {
				continue
			}

			if strings.Contains(v, "|->") {
				nextWordIsID = true
				continue
			}

			if nextWordIsID {
				nextWordIsID = false
				continue
			}

			var builder strings.Builder

			for _, w := range split[i:] {
				builder.WriteString(w)

				if i <= len(split)-2 {
					builder.WriteRune(' ')
				}
			}

			ret = append(ret, builder.String())
			break
		} else {
			if i == 0 {
				continue
			}
			var builder strings.Builder

			for _, w := range split[i:] {
				builder.WriteString(w)

				if i <= len(split)-2 {
					builder.WriteRune(' ')
				}
			}

			ret = append(ret, strings.TrimRight(builder.String(), " "))
			break
		}
	}

	return ret
}

func pwSources(links []Link) []Port {
	var ports []Port

	out, err := exec.Command("pw-link", "-oI").Output()
	if err != nil {
		log.Fatal(err)
	}

	split := strings.Split(string(out), "\n")

	rxMidi := regexp.MustCompile("Midi-Bridge")
	rxVideo := regexp.MustCompile("v4l2")

	for i := 0; i < len(split); i++ {
		if rxMidi.MatchString(split[i]) || rxVideo.MatchString(split[i]) {
			continue
		}

		// Weird empty line at the end of output idk man
		if len(split[i]) == 0 {
			break
		}

		var thisPort Port

		lineSplit := portPathSplit(split[i])

		thisPort.ID = lineSplit[0]
		thisPort.Path = lineSplit[1]

		for _, v := range links {
			if thisPort.Path == v.Source {
				thisPort.Connected = true
				break
			}

			for _, x := range v.Destinations {
				if thisPort.Path == x.Path {
					thisPort.Connected = true
					break
				}
			}

			if thisPort.Connected {
				break
			}
		}

		ports = append(ports, thisPort)
	}

	return ports
}

func pwInputs(links []Link) []Port {
	var ports []Port

	out, err := exec.Command("pw-link", "-iI").Output()
	if err != nil {
		log.Fatal(err)
	}

	split := strings.Split(string(out), "\n")

	rxMidi := regexp.MustCompile("Midi-Bridge")
	rxVideo := regexp.MustCompile("v4l2")

	for i := 0; i < len(split); i++ {
		if rxMidi.MatchString(split[i]) || rxVideo.MatchString(split[i]) {
			continue
		}

		var thisPort Port

		// Weird empty line at the end of output idk man
		if len(split[i]) == 0 {
			break
		}

		lineSplit := portPathSplit(split[i])

		thisPort.ID = lineSplit[0]
		thisPort.Path = lineSplit[1]

		for _, v := range links {
			if thisPort.Path == v.Source {
				thisPort.Connected = true
				break
			}

			for _, x := range v.Destinations {
				if thisPort.Path == x.Path {
					thisPort.Connected = true
					break
				}
			}

			if thisPort.Connected {
				break
			}
		}

		ports = append(ports, thisPort)
	}

	return ports
}

func pwLinks() []Link {
	var links []Link

	out, err := exec.Command("pw-link", "-lI").Output()
	if err != nil {
		log.Fatal(err)
	}

	rxLink := regexp.MustCompile("^ {1,3}[0-9]")
	rxLinkDest := regexp.MustCompile(`\-\>`)

	split := strings.Split(string(out), "\n")

	for i := 0; i < len(split); i++ {
		if rxLink.MatchString(split[i]) {
			if rxLinkDest.MatchString(split[i+1]) {
				var thisLink Link

				firstLineSplit := linkPathSplit(split[i])

				thisLink.Source = firstLineSplit[0]

				// second line contains the link's ID as well in pos 0
				for {
					if rxLinkDest.MatchString(split[i+1]) {
						var thisDestination Destination

						secondLineSplit := linkPathSplit(split[i+1])

						thisDestination.Path = secondLineSplit[1]
						thisDestination.ID = secondLineSplit[0]

						i++

						thisLink.Destinations = append(thisLink.Destinations, thisDestination)
						continue
					}

					break
				}

				links = append(links, thisLink)

				continue
			}

			i++
		}
	}

	return links
}
