# pwlinker

Scriptable and menu-driven interface for pw-link to make connections via a CLI

## Why

I wanted a more scriptable interface for dealing with stereo pipewire connections.
Mostly to handle me moving my laptop between my desk/studio setup and being mobile or on bluetooth.

Why Go: 
- I like writing in Go more than Bash or Python
    - It would likely be just as easy (if not easier), in another language.

## Installation

No release as of yet

## Usage

There are a few ways you can utilize this tool:
- Use the menu interface to make connections manually
- Use the print flag to print out the desired (CSV) info for scripting (see `example.sh`)

<details>
<summary>Menu</summary>

```
# links the current links
(distek)─(INS)─▶ pwlinker -pl
alsa_input.pci-0000_05_00.6.analog-stereo:capture_FL
    56,Bitwig Studio:Mono In 4
Bitwig Studio:Speakers-L
    67,alsa_output.pci-0000_05_00.6.analog-stereo:playback_FL
Bitwig Studio:Speakers-R
    55,alsa_output.pci-0000_05_00.6.analog-stereo:playback_FR

# disconnect the link between the built-in mic and Bitwig
(distek)─(INS)─▶ pwlinker -d
Disconnect link:
 1) alsa_input.pci-0000_05_00.6.analog-stereo:capture_FL -> Bitwig Studio:Mono In 4
 2) Bitwig Studio:Speakers-L -> alsa_output.pci-0000_05_00.6.analog-stereo:playback_FL
 3) Bitwig Studio:Speakers-R -> alsa_output.pci-0000_05_00.6.analog-stereo:playback_FR
Choice: 1

# verify the link is disconnected
(distek)─(INS)─▶ pwlinker -pl
Links:
Bitwig Studio:Speakers-L
    67,alsa_output.pci-0000_05_00.6.analog-stereo:playback_FL
Bitwig Studio:Speakers-R
    55,alsa_output.pci-0000_05_00.6.analog-stereo:playback_FR
```

</details>

<details>
<summary>Print (for scripting)</summary>

```
# links
(distek)─(INS)─▶ pwlinker -pl
Bitwig Studio:Speakers-L
    67,alsa_output.pci-0000_05_00.6.analog-stereo:playback_FL
Bitwig Studio:Speakers-R
    55,alsa_output.pci-0000_05_00.6.analog-stereo:playback_FR

# source ports
(distek)─(INS)─▶ pwlinker -ps
47,"alsa_output.pci-0000_05_00.6.analog-stereo:monitor_FL",false
49,"alsa_output.pci-0000_05_00.6.analog-stereo:monitor_FR",false
50,"alsa_input.pci-0000_05_00.6.analog-stereo:capture_FL",false
51,"alsa_input.pci-0000_05_00.6.analog-stereo:capture_FR",false
78,"Bitwig Studio:Speakers-L",true
64,"Bitwig Studio:Speakers-R",true

# destination ports
(distek)─(INS)─▶ pwlinker -pD
46,"alsa_output.pci-0000_05_00.6.analog-stereo:playback_FL",true
48,"alsa_output.pci-0000_05_00.6.analog-stereo:playback_FR",true
81,"Bitwig Studio:Mono In",false
79,"Bitwig Studio:Mono In 2",false
59,"Bitwig Studio:Mono In 3",false
60,"Bitwig Studio:Mono In 4",false
```

</details>

## TODO's

- [ ] Add midi selection
- [ ] Allow csv choices in disconnect

