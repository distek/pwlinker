package main

type Link struct {
	Source       string
	Destinations []Destination
}

type Destination struct {
	ID   string
	Path string
}

type Port struct {
	ID        string
	Path      string
	Connected bool
}
