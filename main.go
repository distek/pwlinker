package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"

	"github.com/spf13/pflag"
)

var (
	usageBool   bool
	connectbool bool
	discoBool   bool
	printBool   bool
	sourcesBool bool
	destBool    bool
	linkBool    bool
)

// TODO: Add midi selection
// TODO: Allow csv choices in disconnect

func connect() {
	links := pwLinks()

	sources := pwSources(links)
	destinations := pwInputs(links)

	var source string
	var destination string

	for {
		fmt.Printf("Source port:\t(* = has link)\n")

		choices := make(map[string]string, len(sources)-1)

		for i, v := range sources {
			fmt.Printf("%2d) %s", i+1, v.Path)
			switch v.Connected {
			case true:
				fmt.Printf(" *\n")
			case false:
				fmt.Printf("\n")
			}
			choices[fmt.Sprint(i+1)] = v.ID
		}

		var choice string

		fmt.Printf("Choice: ")
		fmt.Scanln(&choice)

		if choices[choice] != "" {
			source = choices[choice]

			break
		}
	}

	fmt.Println()

	for {
		fmt.Printf("Destination port:\t(* = has link)\n")

		choices := make(map[string]string, len(destinations)-1)

		for i, v := range destinations {
			fmt.Printf("%2d) %s", i+1, v.Path)

			switch v.Connected {
			case true:
				fmt.Printf("*\n")
			case false:
				fmt.Printf("\n")
			}

			choices[fmt.Sprint(i+1)] = v.ID
		}

		var choice string

		fmt.Printf("Choice: ")
		fmt.Scanln(&choice)

		if choices[choice] != "" {
			destination = choices[choice]

			break
		}
	}

	if err := exec.Command("pw-link", source, destination).Run(); err != nil {
		log.Fatal(err)
	}
}

func disconnect() {
	links := pwLinks()

	for {
		fmt.Println("Disconnect link:")

		choices := make(map[string]string, len(links)-1)

		i := 0
		for _, v := range links {
			for _, x := range v.Destinations {
				fmt.Printf("%2d) %s -> %s\n", i+1, v.Source, x.Path)
				choices[fmt.Sprint(i+1)] = x.ID
				i++
			}
		}

		var choice string

		fmt.Printf("Choice: ")
		fmt.Scanln(&choice)

		if choices[choice] != "" {
			// No idea why it would fail, but just in case
			if err := exec.Command("pw-link", "-d", choices[choice]).Run(); err != nil {
				log.Fatal(err)
			}

			break
		}
	}
}

func printOut() {
	links := pwLinks()
	sources := pwSources(links)
	destinations := pwInputs(links)

	if linkBool {
		for _, v := range links {
			fmt.Println(v.Source)

			for _, d := range v.Destinations {
				fmt.Printf("    %s,%s\n", d.ID, d.Path)
			}
		}
	}

	if sourcesBool {
		for _, v := range sources {
			fmt.Printf("%s,%s,%v\n", v.ID, strconv.Quote(v.Path), v.Connected)
		}
	}

	if destBool {
		for _, v := range destinations {
			fmt.Printf("%s,%s,%v\n", v.ID, strconv.Quote(v.Path), v.Connected)
		}
	}
}

func main() {
	switch {
	case usageBool:
		pflag.Usage()
		os.Exit(0)
	case printBool:
		printOut()
	case linkBool:
		fmt.Println("-l is only used with -p")
		os.Exit(1)
	case sourcesBool:
		fmt.Println("-s is only used with -p")
		os.Exit(1)
	case destBool:
		fmt.Println("-D is only used with -p")
		os.Exit(1)
	case connectbool:
		connect()
	case discoBool:
		disconnect()
	default:
		pflag.Usage()
		os.Exit(0)
	}
}

func init() {
	pflag.BoolVarP(&usageBool, "help", "h", false, "print this help")
	pflag.BoolVarP(&connectbool, "connect", "c", false, "connect menu")
	pflag.BoolVarP(&discoBool, "disconnect", "d", false, "disconnect menu")
	pflag.BoolVarP(&printBool, "print", "p", false, "print link, sources, and/or destinations for scripting [-Dls]")
	pflag.BoolVarP(&linkBool, "links", "l", false, "print links info in print")
	pflag.BoolVarP(&sourcesBool, "sources", "s", false, "print sources info in print")
	pflag.BoolVarP(&destBool, "desinations", "D", false, "print destinations info in print")

	pflag.Parse()
}
