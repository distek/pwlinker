#!/bin/bash

options=(
    "bitwig-bluetooth"
    "bitwig-docked"
    "bitwig-nomad"
    # "bitwig-test"
    "quit"
)

unlinkAll() {
    IFS=$'\n'
    inLinks=false

    local links=()
    for i in $(pwlinker -pl); do
        if echo $i | grep "^[a-zA-Z0-9]" >/dev/null && ! echo $i | grep "Links:" >/dev/null; then
            if $inLinks; then
                inLinks=false
            fi

            if echo $i | grep $sourceLeft >/dev/null || echo $i | grep $sourceRight >/dev/null; then
                inLinks=true

                continue
            fi
            
            continue
        fi

        links+=($(echo $i | awk -F"," '{print $1}'))
    done

    for link in ${links[@]}; do
        pw-link -d $link
    done
}

bitwig-bluetooth() {
    outLeft="bluez_output.CC_98_8B_E2_BD_5D.a2dp-sink:playback_FL"
    outRight="bluez_output.CC_98_8B_E2_BD_5D.a2dp-sink:playback_FR"
    sourceLeft="Bitwig.*Speakers-L"
    sourceRight="Bitwig.*Speakers-R"
    
    unlinkAll
    link
}

bitwig-docked() {
    # outLeft="alsa_output.usb-Lenovo_ThinkPad_USB-C_Dock_Audio_000000000000-00.analog-stereo:playback_FL"
    # outRight="alsa_output.usb-Lenovo_ThinkPad_USB-C_Dock_Audio_000000000000-00.analog-stereo:playback_FR"
    outLeft="alsa_output.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00.analog-stereo-output:playback_FL"
    outRight="alsa_output.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00.analog-stereo-output:playback_FR"
    sourceLeft="Bitwig.*Speakers-L"
    sourceRight="Bitwig.*Speakers-R"

    unlinkAll
    link
}

bitwig-test() {
    outLeft="my-tunnel:playback_FL"
    outRight="my-tunnel:playback_FR"
    sourceLeft="Bitwig.*Speakers-L"
    sourceRight="Bitwig.*Speakers-R"

    unlinkAll
    link
}

bitwig-nomad() {
    outLeft="alsa_output.pci-0000_05_00.6.analog-stereo:playback_FL"
    outRight="alsa_output.pci-0000_05_00.6.analog-stereo:playback_FR"
    sourceLeft="Bitwig.*Speakers-L"
    sourceRight="Bitwig.*Speakers-R"
    
    unlinkAll
    link
}

link() {
    IFS=$'\n'
    for i in $(pwlinker -ps); do
        if echo $i | grep $sourceLeft >/dev/null; then
            leftPortID=$(echo $i | awk -F',' '{print $1}')
            continue
        fi

        if echo $i | grep $sourceRight >/dev/null; then
            rightPortID=$(echo $i | awk -F',' '{print $1}')
            continue
        fi
    done

    for i in $(pwlinker -pD); do
        if echo "$i" | grep $outLeft >/dev/null; then
            outLeftPortID=$(echo $i | awk -F',' '{print $1}')
        fi

        if echo "$i" | grep $outRight >/dev/null; then
            outRightPortID=$(echo $i | awk -F',' '{print $1}')
        fi
    done

    pw-link "$leftPortID" "$outLeftPortID"
    pw-link "$rightPortID" "$outRightPortID"
}

quit() {
    exit
}

if [ ! -z $1 ]; then
    eval "${options[$1-1]}"
    exit
fi


PS3="Choice: "
select opt in "${options[@]}"; do
    eval $opt
    exit
done
